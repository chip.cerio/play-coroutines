package com.chipcerio.playcoroutines.service

import com.chipcerio.playcoroutines.api.ApiService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiServiceProvider {
    
    private const val BASE_URL = "https://itunes.apple.com/"
    
    private fun httpClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }
    
    fun provides(): ApiService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(httpClient())
            .build()
            .run { create(ApiService::class.java) }
    }
}