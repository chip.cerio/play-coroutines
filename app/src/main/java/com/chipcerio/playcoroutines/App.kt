package com.chipcerio.playcoroutines

import android.app.Application
import com.chipcerio.playcoroutines.api.ApiService
import com.chipcerio.playcoroutines.service.ApiServiceProvider

class App : Application() {
    
    private lateinit var apiService: ApiService
    
    override fun onCreate() {
        super.onCreate()
        apiService = ApiServiceProvider.provides()
    }
    
    fun service(): ApiService = apiService
}