package com.chipcerio.playcoroutines

import android.util.Log

fun log(msg: String) {
    Log.d("PlayCoroutines", msg)
}

fun loge(e: Throwable, msg: String) {
    Log.e("PlayCoroutines", msg, e)
}