package com.chipcerio.playcoroutines.data

data class Response(
    val resultCount: Int = 0,
    val results: List<Item> = emptyList()
)

data class Item(
    val trackId: Int = 0,
    val artistName: String = "",
    val trackName: String = ""
)