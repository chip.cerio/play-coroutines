package com.chipcerio.playcoroutines.data

import com.chipcerio.playcoroutines.api.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class Repository(private val apiService: ApiService) : DataSource {
    
    override suspend fun search(query: String): Response = coroutineScope {
        withContext(Dispatchers.IO) {
            apiService.search(query)
        }
    }
}