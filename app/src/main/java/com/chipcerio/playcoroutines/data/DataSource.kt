package com.chipcerio.playcoroutines.data

interface DataSource {

    suspend fun search(query: String): Response
}