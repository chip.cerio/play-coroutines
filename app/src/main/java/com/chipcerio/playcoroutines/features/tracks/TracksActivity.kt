package com.chipcerio.playcoroutines.features.tracks

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.chipcerio.playcoroutines.App
import com.chipcerio.playcoroutines.R
import com.chipcerio.playcoroutines.data.Item
import com.chipcerio.playcoroutines.di.Injection
import com.chipcerio.playcoroutines.log
import com.chipcerio.playcoroutines.loge
import kotlinx.android.synthetic.main.activity_main.*

class TracksActivity : AppCompatActivity(), ViewModel.Callback {
    
    private lateinit var viewModel: ViewModel
    private lateinit var tracksAdapter: TracksAdapter
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val apiService = (application as App).service()
        val dataSrc = Injection.providesDataSource(apiService)
        viewModel = ViewModel(dataSrc, this)
        initRecyclerView()
    }
    
    override fun onStart() {
        super.onStart()
        viewModel.search("jack+johnson")
    }
    
    override fun onStop() {
        super.onStop()
        viewModel.clear()
    }
    
    private fun initRecyclerView() {
        tracksAdapter = TracksAdapter()
        recyclerview.apply {
            layoutManager = LinearLayoutManager(this@TracksActivity)
            adapter = tracksAdapter
        }
    }
    
    override fun onSuccess(items: List<Item>) {
        tracksAdapter.setItems(items)
    }
    
    override fun onComplete() {
        log("request completed")
    }
    
    override fun onError(err: Throwable) {
        loge(err, "something went wrong")
    }
}
