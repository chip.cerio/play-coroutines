package com.chipcerio.playcoroutines.features.tracks

import com.chipcerio.playcoroutines.data.DataSource
import com.chipcerio.playcoroutines.data.Item
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class ViewModel(private val dataSrc: DataSource, private val callback: Callback?) {
    
    private val scope = CoroutineScope(Dispatchers.Main)
    
    fun search(query: String) {
        scope.launch {
            val result = async { dataSrc.search(query) }
            val response = result.await()
            callback?.onSuccess(response.results)
            callback?.onComplete()
        }
    }
    
    fun clear() = scope.cancel()
    
    interface Callback {
        fun onSuccess(items: List<Item>)
        fun onComplete()
        fun onError(err: Throwable)
    }
}