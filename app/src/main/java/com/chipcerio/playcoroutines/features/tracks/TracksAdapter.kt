package com.chipcerio.playcoroutines.features.tracks

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chipcerio.playcoroutines.data.Item

class TracksAdapter : RecyclerView.Adapter<TracksAdapter.ViewHolder>() {
    
    private var items: List<Item> = emptyList()
    
    override fun getItemCount(): Int = items.size
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        return ViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }
    
    fun setItems(newItems: List<Item>) {
        items = newItems
        notifyDataSetChanged()
    }
    
    class ViewHolder(containerView: View) :
        RecyclerView.ViewHolder(containerView) {
        
        private var view: TextView = containerView.findViewById(android.R.id.text1)
        
        fun bind(item: Item) {
            view.text = item.trackName
        }
    }
}