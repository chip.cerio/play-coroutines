package com.chipcerio.playcoroutines.api

import com.chipcerio.playcoroutines.data.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    // https://itunes.apple.com/search?term=jack+johnson
    @GET("search")
    suspend fun search(
        @Query("term") query: String
    ): Response
}