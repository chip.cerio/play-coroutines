package com.chipcerio.playcoroutines.di

import com.chipcerio.playcoroutines.api.ApiService
import com.chipcerio.playcoroutines.data.DataSource
import com.chipcerio.playcoroutines.data.Repository

class Injection {
    
    companion object {
        fun providesDataSource(
            apiService: ApiService
        ): DataSource = Repository(apiService)
    }
}